//
//  core.hpp
//  core
//
//  Created by Tamás Zahola on 2018. 09. 08..
//  Copyright © 2018. Tamás Zahola. All rights reserved.
//

#ifndef core_hpp
#define core_hpp

#include <glm/glm.hpp>

#if __has_include(<optional>)
#include <optional>
#else
#include <experimental/optional>
namespace std { using namespace experimental; }
#endif

#include <vector>
#include <array>
#include <istream>
#include <ostream>
#include <fstream>
#include <exception>

namespace zz {
namespace core {
    
class mesh {
public:
    using vertex = glm::dvec3;
    using normal = glm::dvec3;
    using tex_coord = glm::dvec3;
    
    using vertex_ref = int;
    using normal_ref = int;
    using tex_coord_ref = int;
    
    using transformation = glm::dmat4x4;
    void transform(const transformation& t);
    
    class face {
        friend class mesh;
        std::vector<vertex_ref> _vertices;
        std::optional<std::vector<normal_ref>> _normals;
        std::optional<std::vector<tex_coord_ref>> _tex_coords;
    public:
        face(std::vector<vertex_ref> vertices,
             std::optional<std::vector<normal_ref>> normals,
             std::optional<std::vector<tex_coord_ref>> tex_coords)
        : _vertices(vertices), _normals(normals), _tex_coords(tex_coords) {
            assert(_normals == std::nullopt || _vertices.size() == _normals->size());
            assert(_tex_coords == std::nullopt || _vertices.size() == _tex_coords->size());
        }
        
        const std::vector<vertex_ref>& vertices() const { return _vertices; };
        const std::optional<std::vector<normal_ref>>& normals() const { return _normals; };
        const std::optional<std::vector<tex_coord_ref>>& tex_coords() const { return _tex_coords; };
    };
    
    class triangle {
        std::array<vertex_ref, 3> _vertices;
        std::optional<std::array<normal_ref, 3>> _normals;
        std::optional<std::array<tex_coord_ref, 3>> _tex_coords;
    public:
        const std::array<vertex_ref, 3>& vertices() const { return _vertices; }
        const std::optional<std::array<normal_ref, 3>>& normals() const { return _normals; }
        const std::optional<std::array<tex_coord_ref, 3>>& tex_coords() const { return _tex_coords; }
        
        triangle(const std::array<vertex_ref, 3>& vertices,
                 const std::optional<std::array<normal_ref, 3>>& normals,
                 const std::optional<std::array<tex_coord_ref, 3>>& tex_coords)
        : _vertices(vertices), _normals(normals), _tex_coords(tex_coords) {
        }
    };
    
    std::vector<triangle> triangulate() const;
    
    double surface_area() const;
    
private:
    std::vector<vertex> _vertices;
    std::vector<normal> _normals;
    std::vector<tex_coord> _tex_coords;
    
    std::vector<face> _faces;
    
public:
    const std::vector<vertex>& vertices() const { return _vertices; }
    const std::vector<normal>& normals() const { return _normals; }
    const std::vector<tex_coord>& tex_coords() const { return _tex_coords; }
    const std::vector<face>& faces() const { return _faces; }
    
    const vertex get_vertex(vertex_ref ref) const { return _vertices[ref]; }
    const normal get_normal(normal_ref ref) const { return _normals[ref]; }
    const tex_coord get_tex_coord(tex_coord_ref ref) const { return _tex_coords[ref]; }
    
    vertex_ref add_vertex(vertex v);
    normal_ref add_normal(normal n);
    tex_coord_ref add_tex_coord(tex_coord t);
    
    void add_face(face f);
};

}
}

#endif /* core_hpp */
