# Setup

Run the `setup` script to get the necessary dependencies (boost & glm):

```
./setup
```

Open the Xcode workspace:

```
open shapr3d_hf.xcworkspace
```

# Structure

The Xcode workspace has 2 projects: *core* and *obj2stl*. 

*core* implements the mesh data structure and the serialization/deserialization routines for OBJ & STL files. 

*core* is built into a static library, which is used by the integration tests (target: *core_integration_tests*)
and the *obj2stl* command-line utility. 

# obj2stl

The *obj2stl* target is a command-line utility intended to convert Wavefron OBJ files to STL files. 

Example usage:

```
./obj2stl -i core/core_integration_tests/resources/airboat.obj -o airboat.stl -s 0.5 1 1
```

This will load the model from *airboat.obj*, shrink it to half its size in the X direction, and write it to *airboat.stl* in STL format (and also print its surface area). 

For more details see the *obj2stl* help:

```
./obj2stl --help
```

# Running the tests

Select the *core_integration_tests* target, and run it like a regular command-line app (Cmd+R). 

