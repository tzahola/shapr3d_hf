//
//  mesh_reader.hpp
//  core
//
//  Created by Tamás Zahola on 2018. 09. 09..
//  Copyright © 2018. Tamás Zahola. All rights reserved.
//

#ifndef mesh_reader_hpp
#define mesh_reader_hpp

#include <core/mesh.hpp>

namespace zz {
namespace core {

class mesh_reader {
public:
    class format_error : public std::runtime_error {
    public:
        format_error() : std::runtime_error("format error") {}
    };
    
    virtual mesh read() = 0; // throws: format_error
};
    
}
}

#endif /* mesh_reader_hpp */
