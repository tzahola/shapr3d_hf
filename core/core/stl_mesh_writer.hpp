//
//  stl_mesh_writer.hpp
//  core
//
//  Created by Tamás Zahola on 2018. 09. 09..
//  Copyright © 2018. Tamás Zahola. All rights reserved.
//

#ifndef stl_mesh_writer_hpp
#define stl_mesh_writer_hpp

#include <core/mesh_writer.hpp>

namespace zz {
namespace core {

class stl_mesh_writer : public mesh_writer {
    std::ostream& _stream;
public:
    stl_mesh_writer(std::ostream& stream) : _stream(stream) {}
    
    virtual void write(const mesh& mesh) override;
};
    
}
}

#endif /* stl_mesh_writer_hpp */
