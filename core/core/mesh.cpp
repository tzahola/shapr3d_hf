//
//  mesh.cpp
//  mesh
//
//  Created by Tamás Zahola on 2018. 09. 08..
//  Copyright © 2018. Tamás Zahola. All rights reserved.
//

#include "mesh.hpp"

#include <glm/gtc/matrix_access.hpp>

namespace zz {
namespace core {

mesh::vertex_ref mesh::add_vertex(mesh::vertex v) {
    _vertices.push_back(v);
    return (vertex_ref)_vertices.size() - 1;
}

mesh::normal_ref mesh::add_normal(mesh::normal n) {
    _normals.push_back(n);
    return (normal_ref)_normals.size() - 1;
}

mesh::tex_coord_ref mesh::add_tex_coord(mesh::tex_coord t) {
    _tex_coords.push_back(t);
    return (tex_coord_ref)_tex_coords.size() - 1;
}

void mesh::add_face(mesh::face f) {
    _faces.push_back(f);
}
    
void mesh::transform(const transformation& t) {
    for (auto& vertex : _vertices) {
        auto transformed_vertex = glm::dvec4(vertex, 1.0) * t;
        vertex = static_cast<glm::dvec3>(transformed_vertex / transformed_vertex.w);
    }
    
    auto determinant = glm::determinant(t);
    if (determinant < 0) {
        for (auto& face : _faces) {
            std::reverse(face._vertices.begin(), face._vertices.end());
        }
    }
    
    auto normal_transform = glm::transpose(glm::inverse(t));
    for (auto& normal : _normals) {
        normal = glm::normalize(static_cast<glm::dvec3>(glm::dvec4(normal, 0.0) * normal_transform));
    }
}
    
static double polygon_area(const std::vector<glm::dvec2>& polygon) {
    // https://en.wikipedia.org/wiki/Shoelace_formula
    double xn_y1 = polygon[polygon.size() - 1].x * polygon[0].y;
    double x1_yn = polygon[polygon.size() - 1].y * polygon[0].x;
    double area = 0;
    for (auto i = 0; i < polygon.size() - 1; ++i) {
        area += (polygon[i].x * polygon[i + 1].y + xn_y1) -
                (polygon[i].y * polygon[i + 1].x - x1_yn);
    }
    area *= 0.5;
    return area;
}
    
double mesh::surface_area() const {
    double surface_area = 0;
    
    for (const auto& face : _faces) {
        const double tolerance = 0.001;
        
        std::optional<glm::dvec3> origin;
        std::optional<glm::dvec3> v1;
        std::optional<glm::dvec3> v2;
        for (auto vertex_ref : face.vertices()) {
            auto vertex = get_vertex(vertex_ref);
            if (origin == std::nullopt) {
                origin = vertex;
            } else if (v1 == std::nullopt) {
                if (glm::length(vertex - *origin) > tolerance) {
                    v1 = vertex;
                }
            } else if (v2 == std::nullopt) {
                if (glm::length(glm::cross(vertex - *origin, *v1 - *origin)) > tolerance) {
                    v2 = vertex;
                }
            } else {
                break;
            }
        }
        
        if (origin != std::nullopt && v1 != std::nullopt && v2 != std::nullopt) {
            glm::dvec3 plane_x = glm::normalize(*v1 - *origin);
            glm::dvec3 plane_y = glm::normalize(glm::cross(glm::cross(plane_x, *v2 - *origin), plane_x));
            glm::dmat2x3 plane_projection = glm::column(glm::column(glm::dmat2x3(), 0, plane_x), 1, plane_y);
            
            std::vector<glm::dvec2> plane_vertices;
            plane_vertices.reserve(face.vertices().size());
            for (auto vertex_ref : face.vertices()) {
                plane_vertices.push_back((get_vertex(vertex_ref) - *origin) * plane_projection);
            }
            
            surface_area += polygon_area(plane_vertices);
        }
    }
    
    return surface_area;
}
    
static int calculate_triangle_count(const mesh& mesh) {
    int triangle_count = 0;
    for (const auto& face : mesh.faces()) {
        triangle_count += std::max<int>(0, (int)face.vertices().size() - 2);
    }
    return triangle_count;
}
    
static mesh::triangle triangle_from_face(const mesh::face& face, int v1, int v2, int v3) {
    std::array<mesh::vertex_ref, 3> vertices = {
        face.vertices()[v1],
        face.vertices()[v2],
        face.vertices()[v3]
    };
    
    std::optional<std::array<mesh::normal_ref, 3>> normals;
    if (face.normals() != std::nullopt) {
        normals = std::array<mesh::normal_ref, 3>{
            (*face.normals())[v1],
            (*face.normals())[v2],
            (*face.normals())[v3]
        };
    }
    
    std::optional<std::array<mesh::tex_coord_ref, 3>> tex_coords;
    if (face.tex_coords() != std::nullopt) {
        tex_coords = std::array<mesh::tex_coord_ref, 3>{
            (*face.tex_coords())[v1],
            (*face.tex_coords())[v2],
            (*face.tex_coords())[v3]
        };
    }
    
    return mesh::triangle(vertices, normals, tex_coords);
}
    
std::vector<mesh::triangle> mesh::triangulate() const {
    std::vector<mesh::triangle> triangles;
    triangles.reserve(calculate_triangle_count(*this));
    for (const auto& face : _faces) {
        if (face.vertices().size() >= 3) {
            for (auto i = 1; i < face.vertices().size() - 1; i++) {
                triangles.push_back(triangle_from_face(face, 0, i, i + 1)); // NB: triangle fan, assumes convex faces!
            }
        }
    }
    return triangles;
}
    
}
}
