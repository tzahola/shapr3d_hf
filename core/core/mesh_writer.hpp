//
//  mesh_writer.hpp
//  core
//
//  Created by Tamás Zahola on 2018. 09. 09..
//  Copyright © 2018. Tamás Zahola. All rights reserved.
//

#ifndef mesh_writer_hpp
#define mesh_writer_hpp

#include <core/mesh.hpp>

namespace zz {
namespace core {

class mesh_writer {
public:
    virtual void write(const mesh& mesh) = 0;
};
    
}
}

#endif /* mesh_writer_hpp */
