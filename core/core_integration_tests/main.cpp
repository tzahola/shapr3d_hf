//
//  core_integration_tests.cpp
//  core
//
//  Created by Tamás Zahola on 2018. 09. 08..
//  Copyright © 2018. Tamás Zahola. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <sstream>

#include <core/mesh.hpp>
#include <core/stl_mesh_writer.hpp>
#include <core/obj_mesh_reader.hpp>
#include <glm/ext.hpp>

#define BOOST_TEST_MODULE core
#include <boost/test/included/unit_test.hpp>

const char* square_obj =
"v 0 0 0\n"
"v 1 0 0\n"
"v 1 1 0\n"
"v 0 1 0\n"
"vn 0 0 1\n"
"f 1//1 2//1 3//1 4//1\n";

const char* triangle_obj =
"v 0 0 0\n"
"v 1 0 0\n"
"v 1 1 0\n"
"vn 0 0 1\n"
"f 1//1 2//1 3//1\n";

const char* triangle_negative_octant_obj =
"v -1 -1 -1\n"
"v 0 -1 -1\n"
"v 0 0 -1\n"
"vn 0 0 1\n"
"f 1//1 2//1 3//1\n";

template<class T> T read_little_endian(std::istream& stream) {
    static_assert(std::is_integral<T>::value && std::is_unsigned<T>::value, "");
    T value = 0;
    for (int i = 0; i < sizeof(T); ++i) {
        char byte = stream.get();
        value |= ((T)byte) << (i * 8);
    }
    return value;
}

static float read_float(std::istream& stream) {
    float value;
    stream.read(reinterpret_cast<char*>(&value), sizeof(value));
    return value;
}

static glm::dvec3 read_stl_vector(std::istream& stream) {
    return glm::dvec3(read_float(stream),
                      read_float(stream),
                      read_float(stream));
}

#define check_vectors_equal(a, b, ...) do { \
    auto aa = (a); \
    auto bb = (b); \
    BOOST_TEST(aa.x == bb.x, __VA_ARGS__); \
    BOOST_TEST(aa.y == bb.y, __VA_ARGS__); \
    BOOST_TEST(aa.z == bb.z, __VA_ARGS__); \
} while (0)

BOOST_AUTO_TEST_SUITE(integration_tests)

BOOST_AUTO_TEST_CASE(cube_surface_area) {
    std::ifstream cube("resources/cube.obj");
    auto mesh = zz::core::obj_mesh_reader(cube).read();
    
    BOOST_TEST(mesh.surface_area() == 6.0, boost::test_tools::tolerance(0.001));
}

BOOST_AUTO_TEST_CASE(normal_transform) {
    std::stringstream square(square_obj);
    auto mesh = zz::core::obj_mesh_reader(square).read();
    mesh.transform(glm::row(glm::identity<glm::dmat4x4>(), 1, glm::dvec4(0, 1, 1, 0)));
    
    BOOST_TEST(mesh.faces().size() == 1);
    
    for (auto i = 0; i < 4; i++) {
        check_vectors_equal(mesh.get_normal((*mesh.faces()[0].normals())[i]),
                            (glm::dvec3(0, -M_SQRT1_2, M_SQRT1_2)),
                            boost::test_tools::tolerance(0.001));
    }
}

BOOST_AUTO_TEST_CASE(mirror_transform) {
    std::stringstream square(square_obj);
    auto mesh = zz::core::obj_mesh_reader(square).read();
    mesh.transform(glm::row(glm::identity<glm::dmat4x4>(), 2, glm::dvec4(0, 0, -1, 0)));
    
    BOOST_TEST(mesh.faces().size() == 1);
    
    auto face = mesh.faces()[0];
    for (auto i = 0; i < 4; i++) {
        check_vectors_equal(mesh.get_normal((*face.normals())[i]), (glm::dvec3(0, 0, -1)), boost::test_tools::tolerance(0.001));
    }
    check_vectors_equal(mesh.get_vertex(face.vertices()[0]), (glm::dvec3(0, 1, 0)), boost::test_tools::tolerance(0.001));
    check_vectors_equal(mesh.get_vertex(face.vertices()[1]), (glm::dvec3(1, 1, 0)), boost::test_tools::tolerance(0.001));
    check_vectors_equal(mesh.get_vertex(face.vertices()[2]), (glm::dvec3(1, 0, 0)), boost::test_tools::tolerance(0.001));
    check_vectors_equal(mesh.get_vertex(face.vertices()[3]), (glm::dvec3(0, 0, 0)), boost::test_tools::tolerance(0.001));
}

BOOST_AUTO_TEST_CASE(stl_conversion) {
    std::stringstream triangle(triangle_obj);
    auto mesh = zz::core::obj_mesh_reader(triangle).read();
    
    std::stringstream stl_output;
    zz::core::stl_mesh_writer stl_writer(stl_output);
    stl_writer.write(mesh);
    
    const int stl_header_length = 80;
    stl_output.ignore(stl_header_length);
    
    std::uint32_t triangle_count = read_little_endian<std::uint32_t>(stl_output);
    BOOST_TEST(triangle_count == 1);
    
    check_vectors_equal(read_stl_vector(stl_output), (glm::dvec3(0, 0, 1)), boost::test_tools::tolerance(0.001));
    
    check_vectors_equal(read_stl_vector(stl_output), (glm::dvec3(0, 0, 0)), boost::test_tools::tolerance(0.001));
    check_vectors_equal(read_stl_vector(stl_output), (glm::dvec3(1, 0, 0)), boost::test_tools::tolerance(0.001));
    check_vectors_equal(read_stl_vector(stl_output), (glm::dvec3(1, 1, 0)), boost::test_tools::tolerance(0.001));
}

BOOST_AUTO_TEST_CASE(stl_positive_octant_rule) {
    std::stringstream triangle(triangle_negative_octant_obj);
    auto mesh = zz::core::obj_mesh_reader(triangle).read();
    
    std::stringstream stl_output;
    zz::core::stl_mesh_writer stl_writer(stl_output);
    stl_writer.write(mesh);
    
    const int stl_header_length = 80;
    stl_output.ignore(stl_header_length);
    
    std::uint32_t triangle_count = read_little_endian<std::uint32_t>(stl_output);
    BOOST_TEST(triangle_count == 1);
    
    check_vectors_equal(read_stl_vector(stl_output), (glm::dvec3(0, 0, 1)), boost::test_tools::tolerance(0.001));
    
    check_vectors_equal(read_stl_vector(stl_output), (glm::dvec3(0, 0, 0)), boost::test_tools::tolerance(0.001));
    check_vectors_equal(read_stl_vector(stl_output), (glm::dvec3(1, 0, 0)), boost::test_tools::tolerance(0.001));
    check_vectors_equal(read_stl_vector(stl_output), (glm::dvec3(1, 1, 0)), boost::test_tools::tolerance(0.001));
}

BOOST_AUTO_TEST_SUITE_END()
