//
//  obj_mesh_reader.hpp
//  core
//
//  Created by Tamás Zahola on 2018. 09. 09..
//  Copyright © 2018. Tamás Zahola. All rights reserved.
//

#ifndef obj_mesh_reader_hpp
#define obj_mesh_reader_hpp

#include <core/mesh_reader.hpp>
#include <istream>

namespace zz {
namespace core {

class obj_mesh_reader : public mesh_reader {
    std::istream& _stream;
public:
    obj_mesh_reader(std::istream& stream) : _stream(stream) {}
    
    virtual mesh read() override;
};
    
}
}

#endif /* obj_mesh_reader_hpp */
