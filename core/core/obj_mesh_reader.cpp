//
//  obj_mesh_reader.cpp
//  core
//
//  Created by Tamás Zahola on 2018. 09. 09..
//  Copyright © 2018. Tamás Zahola. All rights reserved.
//

#include "obj_mesh_reader.hpp"

#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>


using namespace zz::core;

static bool is_whitepace(const std::string& obj_line) {
    return std::find_if_not(obj_line.begin(), obj_line.end(), [](auto c){ return std::isspace(c); }) == obj_line.end();
}

static bool is_comment(const std::string& obj_line) {
    return obj_line[0] == '#';
}

static bool has_prefix(const std::string& string, const std::string& prefix) {
    return string.length() >= prefix.length() && std::equal(string.begin(), string.begin() + prefix.length(),
                                                            prefix.begin(), prefix.end());
}

static bool is_vertex(const std::string& obj_line) {
    return has_prefix(obj_line, "v ");
}

static bool is_normal(const std::string& obj_line) {
    return has_prefix(obj_line, "vn ");
}

static bool is_tex_coord(const std::string& obj_line) {
    return has_prefix(obj_line, "vt ");
}

static bool is_face(const std::string& obj_line) {
    return has_prefix(obj_line, "f ");
}

static mesh::vertex parse_vertex(const std::string& obj_line) {
    assert(is_vertex(obj_line));
    
    double x, y, z;
    
    std::stringstream obj_line_stream(obj_line);
    obj_line_stream.ignore(1);
    obj_line_stream >> std::skipws >> x >> y >> z; // NB: homogeneous coordinates not supported
    if (obj_line_stream.bad() || obj_line_stream.fail()) {
        throw mesh_reader::format_error();
    }
    
    return mesh::vertex(x, y, z);
}

static mesh::normal parse_normal(const std::string& obj_line) {
    assert(is_normal(obj_line));
    
    double x, y, z;
    
    std::stringstream obj_line_stream(obj_line);
    obj_line_stream.ignore(2);
    obj_line_stream >> std::skipws >> x >> y >> z;
    if (obj_line_stream.bad() || obj_line_stream.fail()) {
        throw mesh_reader::format_error();
    }
    
    return mesh::normal(x, y, z);
}


static mesh::tex_coord parse_tex_coord(const std::string& obj_line) {
    assert(is_tex_coord(obj_line));
    
    double u, v, w;
    
    std::stringstream obj_line_stream(obj_line);
    obj_line_stream.ignore(2);
    obj_line_stream >> std::skipws >> u >> v;
    while (obj_line_stream.good() && std::isspace(obj_line_stream.peek())) {
        obj_line_stream.ignore();
    }
    if (obj_line_stream.good()) {
        obj_line_stream >> w;
    } else {
        w = 0.0;
    }
    
    if (obj_line_stream.bad() || obj_line_stream.fail()) {
        throw mesh_reader::format_error();
    }
    
    return mesh::tex_coord(u, v, w);
}

static std::tuple<
    std::vector<int>,
    std::optional<std::vector<int>>,
    std::optional<std::vector<int>>
> parse_face(const std::string& obj_line) {
    assert(is_face(obj_line));
    
    std::vector<int> vertices;
    std::optional<std::vector<int>> tex_coords;
    std::optional<std::vector<int>> normals;
    
    std::stringstream obj_line_stream(obj_line);
    obj_line_stream.ignore(1);
    obj_line_stream >> std::skipws;
    while (true) {
        int vertex;
        if (!(obj_line_stream >> vertex)) {
            break;
        }
        
        std::optional<int> tex_coord;
        if (obj_line_stream.good() && obj_line_stream.peek() == '/') {
            obj_line_stream.ignore(1);
            if (obj_line_stream.peek() == '/') {
                tex_coord = std::nullopt;
            } else {
                int t;
                if (!(obj_line_stream >> t)) {
                    throw mesh_reader::format_error();
                }
                tex_coord = t;
            }
        }
        
        std::optional<int> normal;
        if (obj_line_stream.good() && obj_line_stream.peek() == '/') {
            obj_line_stream.ignore(1);
            int n;
            if (!(obj_line_stream >> n)) {
                throw mesh_reader::format_error();
            }
            normal = n;
        }
        
        if (vertices.empty()) {
            if (normal != std::nullopt) {
                normals = decltype(normals)::value_type();
            }
            if (tex_coord != std::nullopt) {
                tex_coords = decltype(tex_coords)::value_type();
            }
        } else if ((normals == std::nullopt) != (normal == std::nullopt) ||
                   (tex_coords == std::nullopt) != (tex_coord == std::nullopt)) {
            throw mesh_reader::format_error();
        }
        
        vertices.push_back(vertex);
        if (normal != std::nullopt) {
            normals->push_back(*normal);
        }
        if (tex_coord != std::nullopt) {
            tex_coords->push_back(*tex_coord);
        }
    }
    if (!obj_line_stream.eof()) {
        throw mesh_reader::format_error();
    }
    
    return std::make_tuple(vertices, tex_coords, normals);
}

static void fix_obj_indices(std::vector<int>& indices, size_t count) {
    for (auto& index : indices) {
        if (index == 0) {
            throw mesh_reader::format_error();
        } else if (index < 0) {
            index += count;
            if (index < 0) {
                throw mesh_reader::format_error();
            }
        } else {
            index -= 1;
        }
    }
}

mesh obj_mesh_reader::read() {
    mesh mesh;
    
    std::string line;
    while (std::getline(_stream, line)) {
        if (is_whitepace(line) || is_comment(line)) {
            // noop
        } else if (is_vertex(line)) {
            mesh.add_vertex(parse_vertex(line));
        } else if (is_normal(line)) {
            mesh.add_normal(parse_normal(line));
        } else if (is_tex_coord(line)) {
            mesh.add_tex_coord(parse_tex_coord(line));
        } else if (is_face(line)) {
            std::vector<int> vertices;
            std::optional<std::vector<int>> tex_coords;
            std::optional<std::vector<int>> normals;
            std::tie(vertices, tex_coords, normals) = parse_face(line);
            
            fix_obj_indices(vertices, mesh.vertices().size());
            if (tex_coords != std::nullopt) {
                fix_obj_indices(*tex_coords, mesh.tex_coords().size());
            }
            if (normals != std::nullopt) {
                fix_obj_indices(*normals, mesh.normals().size());
            }
            
            mesh.add_face(mesh::face(vertices, normals, tex_coords));
        } else {
            std::cerr << "Unsupported OBJ directive: " << line << std::endl;
        }
    }
    
    return mesh;
}
