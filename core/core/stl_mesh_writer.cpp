//
//  stl_mesh_writer.cpp
//  core
//
//  Created by Tamás Zahola on 2018. 09. 09..
//  Copyright © 2018. Tamás Zahola. All rights reserved.
//

#include "stl_mesh_writer.hpp"


using namespace zz::core;

template<class T> static void write_little_endian(T value, std::ostream& stream) {
    static_assert(std::is_integral<T>::value && std::is_unsigned<T>::value, "");
    for (auto i = 0; i < sizeof(T); i++) {
        stream.put(value & 0xFF);
        value >>= 8;
    }
}

static void write_stl_floats(float x, float y, float z, std::ostream& stream) {
    static_assert(sizeof(float) == 4, "");
    stream.write(reinterpret_cast<char*>(&x), sizeof(float));
    stream.write(reinterpret_cast<char*>(&y), sizeof(float));
    stream.write(reinterpret_cast<char*>(&z), sizeof(float));
}

static void write_stl_dvec3(glm::dvec3 v, std::ostream& stream) {
    write_stl_floats((float)v.x, (float)v.y, (float)v.z, stream);
}

void stl_mesh_writer::write(const mesh& mesh) {
    std::vector<char> file_header(80, 0x00);
    
    auto triangles = mesh.triangulate();
    _stream.write(file_header.data(), file_header.size());
    write_little_endian<std::uint32_t>((std::uint32_t)triangles.size(), _stream);
    
    glm::dvec3 stl_positive_octant_translation(0, 0, 0);
    for (const auto& triangle : triangles) {
        auto v1 = mesh.get_vertex(triangle.vertices()[0]);
        auto v2 = mesh.get_vertex(triangle.vertices()[1]);
        auto v3 = mesh.get_vertex(triangle.vertices()[2]);
        
        stl_positive_octant_translation.x = std::min<double>({ stl_positive_octant_translation.x, v1.x, v2.x, v3.x });
        stl_positive_octant_translation.y = std::min<double>({ stl_positive_octant_translation.y, v1.y, v2.y, v3.y });
        stl_positive_octant_translation.z = std::min<double>({ stl_positive_octant_translation.z, v1.z, v2.z, v3.z });
    }
    
    for (const auto& triangle : triangles) {
        mesh::vertex v1 = mesh.get_vertex(triangle.vertices()[0]);
        mesh::vertex v2 = mesh.get_vertex(triangle.vertices()[1]);
        mesh::vertex v3 = mesh.get_vertex(triangle.vertices()[2]);
        glm::dvec3 normal = glm::normalize(glm::cross(v2 - v1, v3 - v1));
        
        write_stl_dvec3(normal, _stream);
        write_stl_dvec3(v1 - stl_positive_octant_translation, _stream);
        write_stl_dvec3(v2 - stl_positive_octant_translation, _stream);
        write_stl_dvec3(v3 - stl_positive_octant_translation, _stream);
        
        std::array<char, 2> attribute_byte_count = { 0x00, 0x00 };
        _stream.write(attribute_byte_count.data(), attribute_byte_count.size());
    }
}
