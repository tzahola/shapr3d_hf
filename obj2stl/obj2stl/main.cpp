//
//  main.cpp
//  obj2stl
//
//  Created by Tamás Zahola on 2018. 09. 09..
//  Copyright © 2018. Tamás Zahola. All rights reserved.
//

#include <iostream>
#include <string>

#if __has_include(<optional>)
#include <optional>
#else
#include <experimental/optional>
namespace std { using namespace experimental; }
#endif

#include <glm/ext.hpp>
#include <core/stl_mesh_writer.hpp>
#include <core/obj_mesh_reader.hpp>

static void print_help() {
    std::cout
        << "Synopsis: obj2stl -i obj_file -o stl_file [transforms]\n"
        << "Options:\n"
        << "    -i, --input obj_file: input Wavefrom OBJ file\n"
        << "    -o, --output stl_file: output STL file\n"
        << "    -t, --translate tx ty tz: translate model by tx, ty, tz\n"
        << "    -r, --rotate d ax ay az: rotate model by d degrees around axis ax, ay, az\n"
        << "    -s, --scale sx sy sz: scale model by sx, sy, sz in the x, y, z directions\n"
        << std::endl;
}

int main(int argc, const char * argv[]) {
    std::optional<std::string> input_file;
    std::optional<std::string> output_file;
    std::optional<glm::dmat4x4> transform;
    
    for (auto i = 1; i < argc; ++i) {
        std::string arg(argv[i]);
        if (arg == "-h" || arg == "--help") {
            print_help();
            return 0;
        } else if (arg == "-i" || arg == "--input") {
            ++i;
            if (i < argc) {
                input_file = std::string(argv[i]);
            } else {
                break;
            }
        } else if (arg == "-o" || arg == "--output") {
            ++i;
            if (i < argc) {
                output_file = std::string(argv[i]);
            } else {
                break;
            }
        } else if (arg == "-t" || arg == "--translate") {
            double translation[3];
            for (auto j = 0; j < sizeof(translation) / sizeof(translation[0]); ++j, ++i) {
                ++i;
                if (i < argc) {
                    translation[j] = atof(argv[i]);
                } else {
                    std::cerr << "Invalid translation!" << std::endl;
                    return -3;
                }
            }
            transform = glm::translate(transform.value_or(glm::identity<glm::dmat4x4>()),
                                       glm::dvec3(translation[0], translation[1], translation[2]));
        } else if (arg == "-r" || arg == "--rotate") {
            double rotation[4];
            for (auto j = 0; j < sizeof(rotation) / sizeof(rotation[0]); ++j) {
                ++i;
                if (i < argc) {
                    rotation[j] = atof(argv[i]);
                } else {
                    std::cerr << "Invalid rotation!" << std::endl;
                    return -3;
                }
            }
            transform = glm::rotate(transform.value_or(glm::identity<glm::dmat4x4>()),
                                    rotation[0] * M_PI / 180.0,
                                    glm::dvec3(rotation[1], rotation[2], rotation[3]));
        } else if (arg == "-s" || arg == "--scale") {
            double scale[3];
            for (auto j = 0; j < sizeof(scale) / sizeof(scale[0]); ++j) {
                ++i;
                if (i < argc) {
                    scale[j] = atof(argv[i]);
                } else {
                    std::cerr << "Invalid scaling!" << std::endl;
                    return -3;
                }
            }
            transform = glm::scale(transform.value_or(glm::identity<glm::dmat4x4>()),
                                   glm::dvec3(scale[0], scale[1], scale[2]));
        } else {
            std::cerr << "Unrecognized option: " << arg << std::endl;
            return -4;
        }
    }
    
    if (input_file == std::nullopt) {
        std::cerr << "No input file specified!" << std::endl;
        return -1;
    } else if (output_file == std::nullopt) {
        std::cerr << "No output file specified!" << std::endl;
        return -2;
    }
    
    std::optional<zz::core::mesh> mesh;
    {
        std::ifstream input(*input_file);
        zz::core::obj_mesh_reader reader(input);
        try {
            mesh = reader.read();
        } catch (zz::core::mesh_reader::format_error) {
            std::cerr << "Invalid OBJ file!" << std::endl;
            return -5;
        }
    }
    
    if (transform != std::nullopt) {
        mesh->transform(*transform);
    }
    
    {
        std::ofstream output(*output_file);
        zz::core::stl_mesh_writer writer(output);
        writer.write(*mesh);
    }
    
    std::cerr << "Mesh surface area: " << mesh->surface_area() << std::endl;
    
    return 0;
}
